---
layout: post
title:  "첫 블로그"
date:   2021-11-19 15:11:00 +0900
categories: jekyll update
---

# Gitlap에 Jekyll을 이용해서 블로그 구성 후 처음 남기는 포스팅

이런저런 우여곡절이 있었지만 잘 넘기고 Gitlap Page를 구성해서 Jekyll블로그를 구성했다. 

마크 다운 문법도 아직 익숙치 않긴 하지만 계속 포스팅을 하다보면 늘거라 생각한다. 일단 Gitlap Page를 구성하면서 생겼던 난관(?)이나 작업들을 복기하면서 포스팅을 해볼예정이다.

프로그래밍 언어로 치면 이제 **Hello World!** 정도 띄운것이지만 시작이 반 ㅋㅋ