---
layout: post
title:  "지킬 튜토리얼 스터디용 포스트"
date:   2021-11-19 17:24:00 +0900
categories: jekyll update
---

## 지킬 Doc에 있는 튜토리얼들을 처리하는 블로그 페이지

마크다운 스터디를 바로 하고 싶긴하지만 우선 필요할때 하나씩 찾아서 처리하는것으로 하고 우선 Jekyll로 블로그를 구성하는 것이니 우선 사용법부터 익혀본다. 각 사용법에 관련된 것은 이 블로그에 하나하나 남겨둘 예정.. 이 작업이 완료되면 Gitlab Page를 구성했을때의 복기를 남길 생각

---
## 블로그에 Liquid 사용

Jeykll을 이용해서 블로그를 꾸미면 마크다운 안에 Liquid 템플릿 언어를 추가해해서 블로그를 꾸미는게 가능하다. 아래와 같이 마크다운과 섞어서 사용하는것도 가능

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

이 내용을 적용하면 아래와 같이 출력된다.
````
```{% raw %}
{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}
{% endraw %}```
````


``` **Bold** ``` => **Bold**